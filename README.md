# YETI
---

Yeti is a Drupal 8 theme built upon Zurb Foundation 6.

## INSTALLATION

1. **The terminal way:**

    To install Yeti via Composer run:
    ```bash
      composer require drupal/yeti
    ```
    and then:
    ```bash
      drupal theme:install yeti --set-default
    ```
      or
    ```bash
      drush en yeti
    ```

2. **The UI way**

    Go to admin/appearance and install the theme using the UI.

    For more information about this method refer to the [official documentation page](https://www.drupal.org/docs/8/extending-drupal-8/installing-themes)

## USAGE

#### Compile project
To compile *.sass* files Yeti uses [gulp-sass](https://github.com/dlmanning/gulp-sass). All the gulp tasks are already present inside the [gulpfile.js](./gulpfile.js) in the project root. All these tasks can be run at once with:
```bash
  gulp build
```

If you want to use Yeti as a base for your project, edit the files inside *./sass/components* folder.

## SUBTHEME

To generate a Drupal 8 Yeti subtheme run the following command in your terminal

## DEV TOOLS

The tools used to develop Yeti are:
  - [yarn](www.yarnpkg.com)
  - [gulp](www.gulpjs.com)

## DIRECTORY STRUCTURE

The folder structure tries to represent the different stylesheets types used by a theme in Drupal 8.4.x .

Styles files are stored inside two env-dependent directories:
  - **./sass**  <- for development files
  - **./css**   <- for production files

Inside these directories you can find the same folder structure:
> ##### Note: Change [ENV-DIR] with one of the folders above.

- **[ENV-DIR]/base**:
  This folder contains all the styles for the base elements

- **[ENV-DIR]/layout**:
  This folder contains layout files, such as the grid files.

- **[ENV-DIR]/components**:
  This folder contains the UI components

- **[ENV-DIR]/theme**:
  This folder contains the theme files, such as color files.

## DEPENDENCIES LOGIC

By default, Yeti loads only two libraries (yeti_main and yeti_main.icons). You can find these info inside **yeti.info.yml**:
```yaml
  libraries:
    - yeti/yeti_main
    - yeti/yeti_main.icons
```
Every other library is attached to templates using **Twig** *attach_library()* function at the beginning of template.
```twig
  {{ attach_library('yeti/some_library') }}
```
Drupal will take care of the rest during rendering.

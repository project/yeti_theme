/**
 * 1 - Gulp plugins.
 */
const gulp = require('gulp');
const concat = require('gulp-concat');
const ico = require('gulp-to-ico');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const watch = require('gulp-watch');

/**
 * 2 - Paths.
 */
const COLORDIRECTORY = './color';
const MODULEDIRECTORY = './node_modules/';
const SCRIPTDIRECTORY = './js';
const STYLEDIRECTORY = './css';

/**
 * 3 - Vendors paths.
 */
const bourbon = require('bourbon').includePaths;

const FOUNDATIONBASE = `${MODULEDIRECTORY}foundation-sites/`;
const FOUNDATIONPLUGINS = `${FOUNDATIONBASE}dist/js/plugins/`;
const STYLEINCLUDES = [
  `${FOUNDATIONBASE}scss`,
  `${FOUNDATIONBASE}scss/components`,
  `${FOUNDATIONBASE}scss/forms`,
  `${FOUNDATIONBASE}scss/grid`,
  './node_modules/motion-ui',
  './node_modules/foundation-icons/scss',
  './node_modules/material-shadows',
  bourbon,
];

/**
 * 3 - Functions.
 */

const compileSass = (src, include, style, dest) => {
  if (style === 'compressed') {
    gulp.src(src)
      .pipe(sass({
        includePaths: include,
        outputStyle: style,
      }).on('error', sass.logError))
      .pipe(rename({
        extname: '.min.css',
      }))
      .pipe(gulp.dest(dest));
  }
  else {
    gulp.src(src)
      .pipe(sass({
        includePaths: include,
        outputStyle: style,
      }).on('error', sass.logError))
      .pipe(gulp.dest(dest));
  }
};

/**
 * 4 - Common tasks.
 */

// Generates favicon.ico from .png files stored in assets/icons folder.
gulp.task('favicon', () => gulp.src('./assets/icons/*.png')
  .pipe(ico('favicon.ico', { resize: true, sizes: [16, 24, 32, 64, 128, 256] }))
  .pipe(gulp.dest('./')));

// Compile sass files.
gulp.task('sass', () => compileSass('./sass/**/*.sass', STYLEINCLUDES, 'nested', STYLEDIRECTORY));

// Compile and minify sass files.
gulp.task('sass:min', () => compileSass('./sass/**/*.sass', STYLEINCLUDES, 'compressed', STYLEDIRECTORY));

// Watch and compile sass files.
gulp.task('sass:watch', () => watch('./sass/**/*.sass')
  .pipe(sass({
    includePaths: STYLEINCLUDES,
    outputStyle: 'nested',
  }).on('error', sass.logError))
  .pipe(gulp.dest(STYLEDIRECTORY)));

// Watch, compile and minify sass files.
gulp.task('sass:watch:min', () => watch('./sass/**/*.sass')
  .pipe(sass({
    includePaths: STYLEINCLUDES,
    outputStyle: 'compressed',
  }).on('error', sass.logError))
  .pipe(rename({
    extname: '.min.css',
  }))
  .pipe(gulp.dest(STYLEDIRECTORY)));

// Copy foundadtion icons files.
gulp.task('icons:foundation', () => {
  gulp.src([
    './node_modules/foundation-icons/foundation-icons.css',
    './node_modules/foundation-icons/foundation-icons.eot',
    './node_modules/foundation-icons/foundation-icons.svg',
    './node_modules/foundation-icons/foundation-icons.ttf',
    './node_modules/foundation-icons/foundation-icons.woff',
  ])
    .pipe(gulp.dest(`${STYLEDIRECTORY}/fonts`));
});

// Compile Foundation scripts.
gulp.task('foundation:scripts', () => gulp.src([
  `${FOUNDATIONBASE}/dist/js/foundation.js`,
  // FOUNDATIONPLUGINS + '*.js',
  // FOUNDATIONPLUGINS + 'foundation.abide.js',
  // `${FOUNDATIONPLUGINS}foundation.accordion.js`,
  // `${FOUNDATIONPLUGINS}foundation.accordionMenu.js`,
  // `${FOUNDATIONPLUGINS}foundation.core.js`,
  // `${FOUNDATIONPLUGINS}foundation.drilldown.js`,
  // `${FOUNDATIONPLUGINS}foundation.dropdownMenu.js`,
  // FOUNDATIONPLUGINS + 'foundation.equalizer.js',
  // FOUNDATIONPLUGINS + 'foundation.interchange.js',
  // FOUNDATIONPLUGINS + 'foundation.magellan.js',
  // FOUNDATIONPLUGINS + 'foundation.offcanvas.js',
  // FOUNDATIONPLUGINS + 'foundation.orbit.js',
  // FOUNDATIONPLUGINS + 'foundation.positionable.js',
  // `${FOUNDATIONPLUGINS}foundation.responsiveAccordionTabs.js`,
  // `${FOUNDATIONPLUGINS}foundation.responsiveMenu.js`,
  // `${FOUNDATIONPLUGINS}foundation.responsiveToggle.js`,
  // FOUNDATIONPLUGINS + 'foundation.reveal.js',
  // FOUNDATIONPLUGINS + 'foundation.slider.js',
  // FOUNDATIONPLUGINS + 'foundation.smoothScroll.js',
  // FOUNDATIONPLUGINS + 'foundation.sticky.js',
  // FOUNDATIONPLUGINS + 'foundation.tabs.js',
  // FOUNDATIONPLUGINS + 'foundation.toggler.js',
  // FOUNDATIONPLUGINS + 'foundation.tooltip.js',
  // FOUNDATIONPLUGINS + 'foundation.util.box.js',
  // `${FOUNDATIONPLUGINS}foundation.util.core.js`,
  // `${FOUNDATIONPLUGINS}foundation.util.imageLoader.js`,
  // `${FOUNDATIONPLUGINS}foundation.util.keyboard.js`,
  // `${FOUNDATIONPLUGINS}foundation.util.mediaQuery.js`,
  // `${FOUNDATIONPLUGINS}foundation.util.motion.js`,
  // `${FOUNDATIONPLUGINS}foundation.util.nest.js`,
  // `${FOUNDATIONPLUGINS}foundation.timer.js`,
  // `${FOUNDATIONPLUGINS}foundation.util.touch.js`,
  // `${FOUNDATIONPLUGINS}foundation.util.triggers.js`,
  // `${FOUNDATIONPLUGINS}foundation.zf.responsiveAccordion.js`,
])
  .pipe(uglify({
    mangle: false,
  }))
  .pipe(concat('main.js'))
  .pipe(rename({
    extname: '.min.js',
  }))
  .pipe(gulp.dest(SCRIPTDIRECTORY)));

// Color module scripts.
gulp.task('colors:scripts', () => gulp.src('./scripts/color/preview.js')
  .pipe(gulp.dest(COLORDIRECTORY)));

// Compile scripts.
gulp.task('scripts', () => gulp.src(['./scripts/**/*.js', '!./scripts/color/*.js'])
  .pipe(gulp.dest(SCRIPTDIRECTORY)));

// Compile and minify scripts.
gulp.task('scripts:min', () => gulp.src(['./scripts/**/*.js', '!./scripts/color/*.js'])
  .pipe(uglify({
    mangle: false,
  }))
  .pipe(rename({
    extname: '.min.js',
  }))
  .pipe(gulp.dest(SCRIPTDIRECTORY)));

// Watch task for scripts.
gulp.task('scripts:watch', () => watch('./scripts/**/*.js')
  .pipe(gulp.dest(SCRIPTDIRECTORY)));

// Minify watch task for scripts.
gulp.task('scripts:watch:min', () => watch('./scripts/**/*.js')
  .pipe(uglify({
    mangle: false,
  }))
  .pipe(rename({
    extname: '.min.js',
  }))
  .pipe(gulp.dest(SCRIPTDIRECTORY)));

/**
 * 5 - Main tasks.
 */
gulp.task('main', ['favicon', 'foundation']);
gulp.task('assets', ['sass', 'scripts']);
gulp.task('build', ['main', 'assets']);
gulp.task('build:prod', ['sass:min', 'scripts:min', 'main']);
gulp.task('foundation', ['foundation:scripts', 'icons:foundation']);
gulp.task('scripts', ['scripts', 'colors:script']);
gulp.task('watch', ['sass:watch', 'scripts:watch']);
gulp.task('watch:min', ['sass:watch:min', 'scripts:watch']);

// noConfilct mode.
(function ($, Drupal) {
  // Create a new Drupal Behavior to attach on the document.ready().
  Drupal.behaviors.yeti_details = {
    attach: () => {
      $('.details').foundation();
    },
  };
}(jQuery, Drupal));

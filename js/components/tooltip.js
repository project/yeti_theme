// noConfilct mode.
(function ($, Drupal) {
  // Create a new Drupal Behavior to attach on the document.ready().
  Drupal.behaviors.yeti_tooltip = {
    attach: (context, settings) => {
      $('.form-item').foundation();
    }
  }
})(jQuery, Drupal);
'use strict';

// noConfilct mode.
(function ($, Drupal) {
  // Create a new Drupal Behavior to attach on the document.ready().
  Drupal.behaviors.yeti_accordion = {
    attach: function attach(context, settings) {
      $('.accordion').foundation();
    }
  };
})(jQuery, Drupal);
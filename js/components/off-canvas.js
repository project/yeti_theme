'use strict';

// noConfilct mode.
(function ($, Drupal) {
  // Create a new Drupal Behavior to attach on the document.ready().
  Drupal.behaviors.yeti_offcanvas = {
    attach: function attach(context, settings) {
      $('.off-canvas-wrapper').foundation();
    }
  };
})(jQuery, Drupal);
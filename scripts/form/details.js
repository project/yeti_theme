// noConfilct mode.
(function ($, Drupal) {
  // Create a new Drupal Behavior to attach on the document.ready().
  Drupal.behaviors.yeti_details = {
    attach: (context, settings) => {
      $('.details').foundation();
    }
  }
})(jQuery, Drupal);
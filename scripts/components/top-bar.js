// noConfilct mode.
(function ($, Drupal) {
  // Create a new Drupal Behavior to attach on the document.ready().
  Drupal.behaviors.yeti_topbar = {
    attach: (context, settings) => {
      $('#local-tasks').foundation();
      $('#local-tasks-responsive').foundation();
      $('#menubar-responsive').foundation();
      $('#menubar').foundation();
    }
  }
})(jQuery, Drupal);
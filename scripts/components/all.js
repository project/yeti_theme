// noConfilct mode.
(function ($, Drupal) {
  // Create a new Drupal Behavior to attach on the document.ready().
  Drupal.behaviors.yeti = {
    attach: (context, settings) => {
      // Initialize all plugins inside the document element.
      $(document).foundation();
    }
  }
})(jQuery, Drupal);
// noConfilct mode.
(function ($, Drupal) {
  // Create a new Drupal Behavior to attach on the document.ready().
  Drupal.behaviors.yeti_accordion = {
    attach: (context, settings) => {
      $('.accordion').foundation();
    }
  }
})(jQuery, Drupal);
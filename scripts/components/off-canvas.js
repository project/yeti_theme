// noConfilct mode.
(function ($, Drupal) {
  // Create a new Drupal Behavior to attach on the document.ready().
  Drupal.behaviors.yeti_offcanvas = {
    attach: (context, settings) => {
      $('.off-canvas-wrapper').foundation();
    }
  }
})(jQuery, Drupal);
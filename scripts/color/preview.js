/**
 * @file
 * Preview for the Yeti theme.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.color = {
    logoChanged: false,
    callback(context, settings, $form) {
      // Change the logo to be the real one.
      if (!this.logoChanged) {
        $('.color-preview .color-preview-logo img').attr('src', drupalSettings.color.logo);
        this.logoChanged = true;
      }
      // Remove the logo if the setting is toggled off.
      if (drupalSettings.color.logo === null) {
        $('div').remove('.color-preview-logo');
      }

      const COLORPREVIEW = $form.find('.color-preview');
      const COLORCONTENT = $form.find('.preview-content');
      const COLORPALETTE = $form.find('.js-color-palette');
      const COLORPREVIEWBLOCK = COLORPREVIEW.find('.color-preview-sidebar');

      // Link color preview.
      COLORCONTENT.find('.link')
        .css('color', COLORPALETTE.find('input[name="palette[link]"]').val())
        .css('text-shadow-color', COLORPALETTE.find('input[name="palette[shadow]"]').val());

      // Solid background color preview.
      COLORPREVIEW.css('backgroundColor', COLORPALETTE.find('input[name="palette[background]"]').val());

      // Nav link colors.
      COLORPREVIEW.find('.color-preview-navbar-menu')
        .css('background color', COLORPALETTE.find('input[name="palette[base]"]').val())
        .css('text-shadow-color', COLORPALETTE.find('input[name="palette[shadow]"]').val());
      COLORPREVIEW.find('.color-preview-navbar-links')
        .css('color', COLORPALETTE.find('input[name="palette[link-nav]"]').val())
        .css('text-shadow-color', COLORPALETTE.find('input[name="palette[shadow]"]').val());

      // Text preview.
      COLORPREVIEW.find('.color-preview-main h2, .color-preview .preview-content')
        .css('color', COLORPALETTE.find('input[name="palette[text]"]').val())
        .css('text-shadow-color', COLORPALETTE.find('input[name="palette[shadow]"]').val());
      COLORPREVIEW.find('.color-preview-content a')
        .css('color', COLORPALETTE.find('input[name="palette[base]"]').val())
        .css('text-shadow-color', COLORPALETTE.find('input[name="palette[shadow]"]').val());

      // Footer wrapper color preview.
      COLORPREVIEW.find('.color-preview-footer-wrapper')
        .css('background-color', COLORPALETTE.find('input[name="palette[secondary]"]').val());

      // Header color preview.
      COLORPREVIEW.find('.color-preview-header')
        .css('background-color', COLORPALETTE.find('input[name="palette[base]"]').val());

      // Branding color preview.
      COLORPREVIEW.find('.color-preview-site-name')
        .css('color', COLORPALETTE.find('input[name="palette[link-nav]"]').val())
        .css('text-shadow-color', COLORPALETTE.find('input[name="palette[shadow]"]').val());

      // Sidebar block preview.
      COLORPREVIEWBLOCK.css('background-color', COLORPALETTE.find('input[name="palette[background-alt]"]').val());
    },
  };
}(jQuery, Drupal, drupalSettings));

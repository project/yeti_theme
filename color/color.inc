<?php

/**
 * @file
 * Lists available colors and color schemes for Yeti theme.
 */

$info = [
  // Available colors and color labels used in theme.
  'fields' => [
    'alert' => t('Alert'),
    'base' => t('Base'),
    'base-light' => t('Base light'),
    'base-dark' => t('Base dark'),
    'secondary' => t('Secondary'),
    'secondary-light' => t('Secondary light'),
    'secondary-dark' => t('Secondary dark'),
    'black' => t('Black'),
    'white' => t('White'),
    'gray' => t('Gray'),
    'link' => t('Link color'),
    'shadow' => t('Shadow'),
    'success' => t('Success'),
    'text' => t('Text color'),
    'warning' => t('Warning'),
  ],
  // Pre-defined color schemes.
  'schemes' => [
    'default' => [
      'title' => t('Icy (default)'),
      'colors' => [
        'alert' => '#f28072',
        'base' => '#1976d2',
        'base-light' => '#63a4ff',
        'base-dark' => '#004ba0',
        'secondary' => '#b0bec5',
        'secondary-light' => '#e2f1f8',
        'secondary-dark' => '#808e95',
        'black' => '#212121',
        'white' => '#fefefe',
        'gray' => '#e3e3e3',
        'link' => '#0d4b75',
        'shadow' => '#8a8a8a',
        'success' => '#d0f2c7',
        'text' => '#0a0a0a',
        'warning' => '#f2f2a4',
      ],
    ],
    'geek_world' => [
      'title' => t('Geek World'),
      'colors' => [
        'alert' => '#f28072',
        'base' => '#ff6f00',
        'base-light' => '#ffa040',
        'base-dark' => '#c43e00',
        'secondary' => '#303f9f',
        'secondary-light' => '#666ad1',
        'secondary-dark' => '#001970',
        'black' => '#161616',
        'white' => '#fcfcfc',
        'gray' => '#e5e5e5',
        'link' => '#0d4b75',
        'shadow' => '#8a8a8a',
        'success' => '#d0f2c7',
        'text' => '#0a0a0a',
        'warning' => '#f2f2a4',
      ],
    ]
  ],

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => [
    'css/blocks/block--branding-block.min.css',
    'css/blocks/block--search-form-block.min.css',
    'css/components/yeti-accordion-menu.min.css',
    'css/components/yeti-accordion.min.css',
    'css/components/yeti-breadcrumbs.min.css',
    'css/components/yeti-button.min.css',
    'css/components/yeti-callout.min.css',
    'css/components/yeti-drilldown.min.css',
    'css/components/yeti-dropdown-menu.min.css',
    'css/components/yeti-dropdown.min.css',
    'css/components/yeti-form.min.css',
    'css/components/yeti-menu.min.css',
    'css/components/yeti-off-canvas.min.css',
    'css/components/yeti-table.min.css',
    'css/components/yeti-titlebar.min.css',
    'css/components/yeti-topbar.min.css',
    'css/fields/field-image.min.css',
    'css/layout/footer.min.css',
    'css/layout/page.min.css',
    'css/navigation/menu--local-tasks.min.css',
    'css/region/region--breadcrumbs.min.css',
    'css/region/region--end-of-page.min.css',
    'css/region/region--footer-top-center.min.css',
    'css/region/region--footer-top-left.min.css',
    'css/region/region--footer-top-right.min.css',
    'css/region/region--messages.min.css',
    'css/region/region--off-canvas-left.min.css',
    'css/region/region--off-canvas-right.min.css',
    'css/theme/colors.min.css',
    'css/theme/typography.min.css',
    'css/theme/preview.min.css',
  ],

  // Files to copy.
  'copy' => [
    'logo.svg',
  ],

  // Gradient definitions.
  'gradients' => [
    [
      // (x, y, width, height).
      'dimension' => [0, 0, 0, 0],
      // Direction of gradient ('vertical' or 'horizontal').
      'direction' => 'vertical',
      // Keys of colors to use for the gradient.
      'colors' => ['primary', 'secondary'],
    ],
  ],

  // Preview files.
  'preview_library' => 'yeti/color.preview',
  'preview_html' => 'color/preview.html',

  // Attachments.
  '#attached' => [
    'drupalSettings' => [
      'color' => [
        // Put the logo path into JavaScript for the live preview.
        'logo' => theme_get_setting('logo.url', 'yeti'),
      ],
    ],
  ],
];
